const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const nodemailer = require('nodemailer');
const robots = require('express-robots-txt');

const app = express();

const layoutRouter = require('./routes/layout');



// muebleria.isacc20@gmail.com
// muebleria.isaac@hotmail.com
// muebleriaisaac123
var transporter = nodemailer.createTransport({
     service: 'gmail',
    // host: 'smtp.gmail.com',
    // port: 587,
    // secure: true,
    auth: {
        user: 'muebleria.isacc20@gmail.com',
        pass: 'muebleriaisaac123'
    }
});

app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
});

//view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname,'public')));

//cath 404 and forward to error handler
app.use(function(err,req,res,next){
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});

app.use('/', layoutRouter);
app.use('/home',layoutRouter)
app.use('/productos/:id',layoutRouter)

// get all todos
app.post('/send-email', (req, res) => {
    const email = req.body.subject;
    const slug = req.body.slug;
    var message = req.body.message;

    const nombre = req.body.nombre;
    const descripcion = req.body.descripcion;
    const precioMostrar = req.body.precioMostrar;
    
    message = 'De: '+ email+'\n\nMuy buenas,' + '\n\nQuisiera cotizar sobre el producto\nProducto: ' + nombre +'\nSlug: ' +slug+ '\nDescripcion: ' + descripcion + '\nPrecio: $ ' + precioMostrar +'\n\n'+message

    var mailOptions = {
        from: email,
        to: 'muebleria.isacc20@gmail.com',
        subject: 'Precio',
        text: message
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log('-----------------');
            console.log(error);
            console.log('-----------------');
        } else {
          console.log('Email sent: ' + info.response);
          res.redirect("/productos/articulo/" + slug);
        }
    });
    res.redirect("/productos/articulo/" + slug);
});

app.listen(3000, function(){
    console.log('Server started on port 3000');
});