$(document).ready(function () {
    $('.routePage').on('click', routePage);

    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
    }

});

function ofertasGet(){
    $.ajax({
        type: 'GET',
        url: '/productos/ofertas'
    }).done(function (response) {
        window.location.replace("/productos/ofertas" );
    });
}

function routePage(data) {
    $.ajax({
        type: 'GET',
        url: '/productos/' + data
    }).done(function (response) {
        window.location.replace("/productos/articulo/" + data);
    });
}


function sendMail() {
    console.log('button was clicked');
    $.ajax({
        type: 'GET',
        url: '/sendMail/1'
    }).done(function (response) {
        window.location.replace("/sendMail/a");
    });
}

var slideIndex = 1;
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}

function runScript(e) {
    //See notes about 'which' and 'key'
    if (e.keyCode == 13) {
        $.ajax({
            type: 'GET',
            url: '/productos/catalogo/ofertas'
        }).done(function (response) {
            window.location.replace("/productos/catalogo/ofertas");
        });
    }
    else{
    }
}