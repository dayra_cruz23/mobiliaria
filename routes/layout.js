const express = require('express');
const router = express.Router();
var http = require('http');

var articulo = {
  id : 0,
  nombre : '',
  slug : '',
  descripcion : '',
  precioMostrar : 0.0,
  id_tipo_articulo : ''
}

var options = {
  host: '45.33.63.248',
  port: 10200,
  path: '',
  method: 'GET'
}
var tipoArticulos = '';
var sugerencias = '';

/* GET home page. */
router.get('/', function (req, res, next) {
  var body = '';
  //var tipoArticulos;
  var ArticulosDestacados;
  options.path = '/public/view/home';
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      body += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(body);
      ArticulosDestacados = valores.response.destacados;
      tipoArticulos = valores.response.tipoArticulos;
      res.render('home', { title: "Mobiliaria", tipoArticulos: tipoArticulos, destacados: ArticulosDestacados });
    });
  }).end();
});


/* GET acerca de page. */
router.get('/acerca', function (req, res, next) {
  var body = '';
  //var tipoArticulos;
  res.render('acerca');
  }
);


/* GET acerca de page. */
router.get('/sucursales', function (req, res, next) {
  var body = '';
  //var tipoArticulos;
  res.render('sucursales');
  }
);

/* GET ofertas page. */
router.get('/productos/catalogo/:id', function (req, res) {
  var body = '';
  var subTipos = [];
  var articulos;

  getTipoArticulos();

  options.path = '/public/catalogo/slug/' + req.params.id;
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      body += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(body);
      articulos = valores.response.articulos;
      if (req.params.id == "nuevos_productos"){
        req.params.id = "Productos Nuevos"
      }
      res.render('producto', { slug: req.params.id,subTipos: subTipos, articulos: articulos, tipoArticulos: tipoArticulos });
    });
  }).end();
});


router.get('/productos/:id', function (req, res) {
  var body = '';
  var subTipos;
  var articulos;

  getTipoArticulos();

  options.path = '/public/tipoArticulo/slug/' + req.params.id;
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      body += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(body);
      subTipos = valores.response.subTipos;
      articulos = valores.response.articulos;
      res.render('producto', { slug: req.params.id,subTipos: subTipos, articulos: articulos, tipoArticulos: tipoArticulos});
    });
  }).end();
});


router.get('/productos/articulo/:id', function (req, res) {
  var body = '';
  var fotos;
  var caracteristicas;
  var tipoArticulo;
  getTipoArticulos();
  getSugerencias();

  options.path = '/public/articulo/slug/' + req.params.id;
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      body += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(body);
      articulo ={
        id : valores.response.id,
        nombre : valores.response.nombre,
        slug : valores.response.slug,
        precioMostrar : valores.response.precioMostrar,
        descripcion : valores.response.descripcion
      }
      tipoArticulo = valores.response.tipoArticulo;
      fotos = valores.response.fotoArticulos;
      caracteristicas = valores.response.caracteristicas;
      res.render('productoVenta',{tipoArticulos: tipoArticulos, articulo : articulo,fotos:fotos,caracteristicas:caracteristicas, destacados: sugerencias  });
    });
  }).end();
});


router.get('/sendMail/:id', function (req, res) {
  console.log('asdas');
});

function getTipoArticulos(){
  var navbar = '';
  options.path = '/public/view/home';
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      navbar += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(navbar);
      tipoArticulos = valores.response.tipoArticulos;
    });
  }).end();
}

function getSugerencias(){
  var navbar = '';
  options.path = '/public/catalogo/slug/sugerencias';
  http.request(options, function (response) {
    response.on('data', function (chunks) {
      navbar += chunks;
    })
    response.on('end', function () {
      var valores = JSON.parse(navbar);
      sugerencias = valores.response.articulos;
    });
  }).end();
}


module.exports = router;

